rm(list = ls())

# libraries -----------------
library(dplyr, warn.conflicts = FALSE)
library(readxl)
library(tidyr)
library(tibble)
library(stringr)
library(ggplot2)
library(scales)
library(forcats)
library(purrr)

theme <- theme(axis.text.x = element_text(angle = 45, hjust = 1),
               legend.key.size = unit(0.4, "cm"),
               legend.key.width = unit(0.4,"cm"),
               legend.text = element_text(size = 8),
               plot.title = element_text(hjust = 0.5, face = "bold", size = 10),
               panel.grid.major.y = element_line(size = 0.3, color = "grey", linetype = "dashed"))

# DATA_LOLOTREAT <- read_excel("data/DATA-LOLOTREAT3.xlsx", sheet = "Feuil1")
DATA_LOLOTREAT <- read_excel("data/DATA-LOLOTREAT-final.xlsx", sheet = "Feuil1")


jours <- c("Predose", "D1", "D3", "D7", "D10", "D14", "D17", "D21", "D28", "D30", "D35", "D37",
           "D42", "D49", "D56", "D70", "D84", "D98", "D112", "D126", "D140", "D154", "D168")

red_day <- c("D14", "D28", "D42", "D56", "D84","D112","D140", "D168")

dataset <-
  DATA_LOLOTREAT %>%
  select(1, 3, 4, 5, Predose, TtarmmITT4, contains("parasitemi"), name = 1559) %>%
  mutate_at(vars(contains("arasitemi")), ~(as.numeric(.))) %>%
  filter(name == "Yes") %>%
  select(-c(name, `LoaLoa ParasitemiaSCR`)) %>%
  rename(`LoaLoa ParasitemiaSCR` = Predose) %>% 
  pivot_longer(-c(1 : 4, 6), names_to = "variable", values_to = "parasitemia") %>%
  `colnames<-`(c("study_Id", "date_scrining", "gender", "date_birth", "treatment", "variable", "parasitemia")) %>%
  filter(!is.na(parasitemia)) %>%
  separate(variable, into = c("speacesis", "days"))%>%
  mutate(days = str_replace(days, "Parasitemia", ""),
         treatment = case_when(treatment == 1 ~ "Control",
                               treatment == 2 ~ "ALB",
                               treatment == 3 ~ "ALB+ALB",
                               treatment == 4 ~ "ALB+IVM"),
         study_Id = paste0("patient_", study_Id),
         days = if_else(days == "SCR", "Predose", days)) %>%
  filter(days != "D0")


## Etude de Loaloa -------------------------------------------------------------
data_loa_1 <-
  dataset %>%
  filter(speacesis == "LoaLoa") %>%
  group_by(treatment, days) %>%
  summarise(mean_parasite = round(mean(parasitemia, na.rm = T)),
            median_parasite = median(parasitemia)) %>%
  ungroup() %>%
  arrange(factor(days, levels = jours))

# figure 1 ---------------------------------------------------------------------
fig_1 <- 
  data_loa_1 %>%
  ggplot(aes(x = factor(days, levels = jours), y = median_parasite,
             color = fct_reorder2(treatment, factor(days, levels = jours), median_parasite), group = treatment)) +
  geom_point() +
  geom_line(aes(linetype = treatment)) +
  theme_classic() +
  scale_y_continuous(breaks = seq(0, 20000, 2500)) +
  scale_color_manual(values = c("green", "blue", "orange", "red")) +
  # scale_linetype_manual(values=c("twodash", "dotted", "solid", "logdash")) +
  labs(x = " ",
       y = " ",
       color = "",
       linetype = "",
       title = "Figure 1: Evolution médiane des parasitémies de loaloa") +
  theme

# figure 2 ---------------------------------------------------------------------
fig_2 <- 
  dataset %>%
  filter(speacesis == "LoaLoa", treatment == "Control") %>%
  ggplot(aes(x = factor(days, levels = jours), y = parasitemia, 
             color = fct_reorder2(study_Id, factor(days, levels = jours), parasitemia), group = study_Id)) +
  geom_point() +
  geom_line() +
  theme_classic() +
  scale_y_continuous(breaks = seq(0, 40000, 5000)) +
  scale_color_manual(values = c("green", "blue", "orange", "red", "darkgreen", "darkorange4")) +
  labs(x = " ",
       y = " ",
       color = " ",
       title = "Figure 2: Evolution des parasitémies des parients pour le traitment Control de LoaLoa") +
  theme

# figure 3 ---------------------------------------------------------------------
fig_3 <- 
  dataset %>%
  filter(speacesis == "LoaLoa", treatment == "ALB") %>%
  ggplot(aes(x = factor(days, levels = jours), y = parasitemia,
             color = fct_reorder2(study_Id, factor(days, levels = jours), parasitemia), group = study_Id)) +
  geom_point() +
  geom_line() +
  theme_classic() +
  scale_y_continuous(breaks = seq(0, 40000, 5000)) +
  scale_color_manual(values = c("red", "blue", "orange", "green4", "darkgreen", "darkorange4", "darkmagenta", "tan1",
                                "violet", "gray0", "chartreuse", "chocolate")) +
  labs(x = " ",
       y = " ",
       color = " ",
       title = "Figure 3: Evolution des parasitémies des parients pour le traitment ALB de LoaLoa") +
  theme


# figure 4 ---------------------------------------------------------------------
fig_4 <- 
  dataset %>%
  filter(speacesis == "LoaLoa", treatment == "ALB+ALB") %>%
  ggplot(aes(x = factor(days, levels = jours), y = parasitemia,
             color = fct_reorder2(study_Id, factor(days, levels = jours), parasitemia), group = study_Id)) +
  geom_point() +
  geom_line() +
  theme_classic() +
  scale_y_continuous(breaks = seq(0, 70000, 10000)) +
  scale_color_manual(values = c("red", "blue", "orange", "green4", "darkgreen", "darkorange4", "darkmagenta", "tan1",
                                "violet", "gray0", "chartreuse", "chocolate", "cyan")) +
  labs(x = " ",
       y = " ",
       color = " ",
       title = "Figure 4: Evolution des parasitémies des parients pour le traitment ALB+ALB de LoaLoa") +
  theme


# figure 5 ---------------------------------------------------------------------
  fig_5 <- 
  dataset %>%
  filter(speacesis == "LoaLoa", treatment == "ALB+IVM") %>%
  ggplot(aes(x = factor(days, levels = jours), y = parasitemia,
             color = fct_reorder2(study_Id, factor(days, levels = jours), parasitemia), group = study_Id)) +
  geom_point() +
  geom_line() +
  theme_classic() +
  scale_y_continuous(breaks = seq(0, 30000, 5000)) +
  scale_color_manual(values = c("red", "blue", "orange", "green4", "darkorange4", "darkmagenta",
                                "violet", "chartreuse", "chocolate")) +
  labs(x = " ",
       y = " ",
       color = " ",
       title = "Figure 5: Evolution des parasitémies des parients pour le traitment ALB+IVM de LoaLoa") +
  theme

## Calcule de la reduction des parasitémie -------------------------------------
dataset2 <-
  DATA_LOLOTREAT %>%
  select(1, 3, 4, 5, Predose, TtarmmITT4, contains("parasitemi"), name = 1559) %>%
  mutate_at(vars(contains("arasitemi")), ~(as.numeric(.))) %>%
  filter(name == "Yes") %>%
  select(-c(name, `LoaLoa ParasitemiaSCR`)) %>%
  rename(`LoaLoa ParasitemiaSCR` = Predose) %>% 
  pivot_longer(-c(1 : 7), names_to = "variable", values_to = "parasitemia") %>%
  `colnames<-`(c("study_Id", "date_scrining", "gender", "date_birth", "loa_SCR", "treatment", "mans_SCR", "variable", "parasitemia")) %>%
  filter(!is.na(parasitemia)) %>%
  separate(variable, into = c("speacesis", "days")) %>%
  mutate(days = str_replace(days, "Parasitemia", ""),
         treatment = case_when(treatment == 1 ~ "Control",
                               treatment == 2 ~ "ALB",
                               treatment == 3 ~ "ALB+ALB",
                               treatment == 4 ~ "ALB+IVM"),
         study_Id = paste0("patient_", study_Id))

data_loa_2 <-
  dataset2 %>%
  filter(speacesis == "LoaLoa") %>%
  mutate(day_red = case_when(days == "D14" ~ "D14",
                             days %in% c("D28", "D30") ~ "D28",
                             days == "D42" ~ "D42",
                             days == "D56" ~ "D56",
                             days == "D84" ~ "D84",
                             days == "D112" ~ "D112",
                             days == "D140" ~ "D140",
                             days == "D168" ~ "D168")) %>%
  filter(!is.na(day_red)) %>%
  group_by(study_Id) %>%
  arrange(factor(day_red, levels = red_day)) %>%
  arrange(study_Id) %>%
  ungroup() %>%
  mutate(reduction = round((loa_SCR - parasitemia) / loa_SCR, 2))

table_loa <-
  data_loa_2 %>%
  group_by(treatment, day_red) %>%
  summarise(mean_reduction = round(mean(reduction, na.rm = T), 2),
            median_reduction = median(reduction)) %>%
  ungroup()

tab_1 <-
  table_loa%>%
  pivot_wider(names_from = treatment, values_from = c(mean_reduction, median_reduction)) %>%
  arrange(factor(day_red, levels = red_day)) %>%
  select(1, 2, 6, 3, 7, 4, 8, 5, 9)

# figure 6 ---------------------------------------------------------------------
fig_6 <- 
  table_loa %>%
  ggplot(aes(factor(day_red, levels = red_day), median_reduction,
             color = fct_reorder2(treatment, factor(day_red, levels = red_day), median_reduction), group = treatment)) +
  geom_point() +
  geom_line() +
  theme_classic() +
  scale_color_manual(values = c("green", "blue", "orange", "red")) +
  labs(x = " ",
       y = " ",
       color = " ",
       title = "Figure 6: Variation de la médiane de la reduction des parasitémies de loaloa") +
  theme

# table Mann-Whitney test for Loaloa -------------------------------------------

loa_2 <-
  data_loa_2 %>%
  select(day_red, treatment, reduction) %>%
  filter(treatment %in% c("Control", "ALB")) %>% 
  group_by(treatment, day_red) %>% 
  summarise(reduction = list(reduction))  %>% 
  spread(treatment, reduction) %>% 
  group_by(day_red) %>% 
  mutate(p_value_2 = wilcox.test(unlist(Control), unlist(ALB), exact = F)$p.value,
         Control = map(Control, median),
         ALB = map(ALB, median)) %>%
  mutate_at(vars(Control, ALB), ~(round(as.numeric(.), 2)))

loa_3 <-
  data_loa_2 %>%
  select(day_red, treatment, reduction) %>%
  filter(treatment %in% c("Control", "ALB+ALB")) %>% 
  group_by(treatment, day_red) %>% 
  summarise(reduction = list(reduction))  %>% 
  spread(treatment, reduction) %>% 
  group_by(day_red) %>% 
  mutate(p_value_3 = wilcox.test(unlist(Control), unlist(`ALB+ALB`), exact = F)$p.value,
         Control = map(Control, median),
         `ALB+ALB` = map(`ALB+ALB`, median)) %>%
  mutate_at(vars(Control, `ALB+ALB`), ~(round(as.numeric(.), 2)))

loa_4 <-
  data_loa_2 %>%
  select(day_red, treatment, reduction) %>%
  filter(treatment %in% c("Control", "ALB+IVM")) %>% 
  group_by(treatment, day_red) %>% 
  summarise(reduction = list(reduction))  %>% 
  spread(treatment, reduction) %>% 
  group_by(day_red) %>% 
  mutate(p_value_4 = wilcox.test(unlist(Control), unlist(`ALB+IVM`), exact = F)$p.value,
         Control = map(Control, median),
         `ALB+IVM` = map(`ALB+IVM`, median)) %>%
  mutate_at(vars(Control, `ALB+IVM`), ~(round(as.numeric(.), 2)))


tab_2 <- loa_2 %>%
  arrange(factor(day_red, levels = red_day)) %>%
  left_join(loa_3%>%select(-c(Control)), by = "day_red") %>%
  left_join(loa_4%>%select(-c(Control)), by = "day_red")

# table kruskal test for Loaloa-------------------------------------------------
library(rstatix)

tab_sum <- 
  data_loa_2 %>%
  group_by(day_red, treatment) %>% 
  get_summary_stats(reduction, type = "common") %>%
  mutate(median = round(median, 2)) %>%
  select(day_red, treatment, median) %>%
  pivot_wider(names_from = treatment, values_from = median) %>%
  arrange(factor(day_red, levels = red_day))

tab_kw <-
  data_loa_2  %>%
  select(day_red, treatment, reduction) %>% 
  group_by(day_red) %>% 
  kruskal_test(reduction ~ treatment)%>%
  select(day_red, statistic, p_value = p)

tab_3 <- tab_sum %>%
  left_join(tab_kw, by = "day_red")
## Test de wald ----------------------------------------------------------------
total <-
  DATA_LOLOTREAT %>%
  select(1, 3, 4, 5, treatment = TtarmmITT4, contains("parasitemi"), name = 1558) %>%
  filter(name == "Yes") %>%
  mutate(treatment = case_when(treatment == 1 ~ "Control",
                               treatment == 2 ~ "ALB",
                               treatment == 3 ~ "ALB+ALB",
                               treatment == 4 ~ "ALB+IVM")) %>%
  count(treatment, name = "N")


tab_wald <- 
  dataset %>%
  filter(days == "D168", speacesis == "LoaLoa", parasitemia <= 100) %>%
  count(treatment) %>%
  left_join(total, by = "treatment") %>%
  mutate(perc = round(n / N * 100, 1)) %>% 
  mutate(lower = round(((n/N)-1.96*sqrt(((n/N)*(1-(n/N)))/N))*100, 1),
         upper = round(((n/N)+1.96*sqrt(((n/N)*(1-(n/N)))/N))*100, 1),
         lower = if_else(lower < 0, 0, lower),
         CI = paste0("[", lower, " ; ", upper, "]"))

tab_4 <-
  tab_wald %>% 
  select(treatment, n, N, perc, CI)

## Logistic Regression ---------------------------------------------------------

data_lr <-
  dataset %>% 
  filter(speacesis == "LoaLoa") %>%
  select(days, treatment, parasitemia) %>% 
  mutate(treatment = if_else(treatment == "Control", "  Control", treatment),
         parasitemia = if_else(parasitemia <= 100, 1, 0),
         parasitemia = factor(parasitemia, levels = c(1, 0), labels = c("yes", "no"))) %>% 
  arrange(desc(treatment))


mod1 <- glm(parasitemia ~ treatment, family = binomial(logit), data = data_lr)

summary(mod1)

tab_5 <-
  mod1 %>% 
  summary() %>% 
  coefficients %>% 
  data.frame() %>%
  `colnames<-`(c("estimate", "str_error", "z_value", "p_value")) %>% 
  mutate(OR = signif(exp(estimate), 3),
         p_value = as.character(signif(p_value, 3))) %>% 
  relocate(OR, .after = z_value) %>% 
  select(-c(estimate, str_error, z_value))



## Etude de Mansonella ---------------------------------------------------------
data_mans <-
  dataset %>%
  filter(speacesis == "Mansonella") %>%
  group_by(treatment, days) %>%
  summarise(mean_parasite = round(mean(parasitemia, na.rm = T)),
            median_parasite = median(parasitemia),
            Geo_mean = round(exp(mean(log(parasitemia))))) %>%
  ungroup() %>%
  arrange(factor(days, levels = jours))

# figure 7 ---------------------------------------------------------------------

fig_7 <- 
  data_mans %>%
  ggplot(aes(x = factor(days, levels = jours), y = median_parasite, 
             color = fct_reorder2(treatment, factor(days, levels = jours), median_parasite), group = treatment)) +
  geom_point() +
  geom_line() +
  theme_classic() +
  scale_y_continuous(breaks = seq(0, 5000, 500)) +
  scale_color_manual(values = c("green", "blue", "orange", "red")) +
  labs(x = " ",
       y = " ",
       color = " ",
       title = "Figure 7: Evolution médiane des parasitémies de mansonella") +
  theme

# figure 8 ---------------------------------------------------------------------
fig_8 <- 
  dataset %>%
  filter(speacesis == "Mansonella", treatment == "Control") %>%
  ggplot(aes(x = factor(days, levels = jours), y = parasitemia, 
             color = fct_reorder2(study_Id, factor(days, levels = jours), parasitemia), group = study_Id)) +
  geom_point() +
  geom_line() +
  theme_classic() +
  scale_y_continuous(breaks = seq(0, 5000, 500)) +
  scale_color_manual(values = c("green", "blue", "orange", "red", "darkgreen", "darkorange4")) +
  labs(x = " ",
       y = " ",
       color = " ",
       title = "Figure 8: Evolution des parasitémies des parients pour le traitment Control de Mansonella") +
  theme

# figure 9 ---------------------------------------------------------------------
fig_9 <- 
  dataset %>%
  filter(speacesis == "Mansonella", treatment == "ALB") %>%
  ggplot(aes(x = factor(days, levels = jours), y = parasitemia,
             color = fct_reorder2(study_Id, factor(days, levels = jours), parasitemia), group = study_Id)) +
  geom_point() +
  geom_line() +
  theme_classic() +
  scale_y_continuous(breaks = seq(0, 5000, 50)) +
  scale_color_manual(values = c("red", "blue", "orange", "green4", "darkgreen", "darkorange4", "darkmagenta", "tan1",
                                "violet", "gray0", "chartreuse", "chocolate")) +
  labs(x = " ",
       y = " ",
       color = " ",
       title = "Figure 9: Evolution des parasitémies des parients pour le traitment ALB de Mansonella") +
  theme


# figure 10 ---------------------------------------------------------------------
fig_10 <- 
  dataset %>%
  filter(speacesis == "Mansonella", treatment == "ALB+ALB") %>%
  ggplot(aes(x = factor(days, levels = jours), y = parasitemia,
             color = fct_reorder2(study_Id, factor(days, levels = jours), parasitemia), group = study_Id)) +
  geom_point() +
  geom_line() +
  theme_classic() +
  scale_y_continuous(breaks = seq(0, 5000, 50)) +
  scale_color_manual(values = c("red", "blue", "orange", "green4", "darkgreen", "darkorange4", "darkmagenta", "tan1",
                                "violet", "gray0", "chartreuse", "chocolate", "cyan")) +
  labs(x = " ",
       y = " ",
       color = " ",
       title = "Figure 10: Evolution des parasitémies des parients pour le traitment ALB+ALB de Mansonella") +
  theme


# figure 11 ---------------------------------------------------------------------
fig_11 <- 
  dataset %>%
  filter(speacesis == "Mansonella", treatment == "ALB+IVM") %>%
  ggplot(aes(x = factor(days, levels = jours), y = parasitemia,
             color = fct_reorder2(study_Id, factor(days, levels = jours), parasitemia), group = study_Id)) +
  geom_point() +
  geom_line() +
  theme_classic() +
  scale_y_continuous(breaks = seq(0, 5000, 50)) +
  scale_color_manual(values = c("red", "blue", "orange", "green4", "darkorange4", "darkmagenta",
                                "violet", "chartreuse", "chocolate")) +
  labs(x = " ",
       y = " ",
       color = " ",
       title = "Figure 11: Evolution des parasitémies des parients pour le traitment ALB+IVM de Mansonella") +
  theme


## Change this only if you need to change location of the report ---------------

reportfolder <- "reports/"

filereportRmd <- "child_lolotreat.Rmd"

output_DIR <- paste0(reportfolder, "Rapport_lolotreat_", format(Sys.Date(), "%Y-%m-%d"))

rmarkdown::render(filereportRmd, output_file = output_DIR, quiet = TRUE)

# End --------------------------------------------------------------------------

